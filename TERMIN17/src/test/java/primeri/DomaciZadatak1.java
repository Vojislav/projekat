package primeri;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DomaciZadatak1 {

	private WebDriver browser;
	
	@BeforeMethod
	public void seleniumSetUp(){
		browser = new FirefoxDriver();
		browser.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		browser.manage().window().maximize();
		browser.navigate().to("localhost:9000/#/");
	}

	@Test
	public void logIn() throws InterruptedException{
		browser.findElement(By.id("account-menu")).click();
		browser.findElement(By.cssSelector("#navbar-collapse > ul > li.dropdown.pointer.open > ul > li:nth-child(1) > a")).click();
		browser.findElement(By.id("username")).sendKeys("admin");
		browser.findElement(By.id("password")).sendKeys("admin");
		browser.findElement(By.xpath("//button[@translate='login.form.button']")).click();
		Thread.sleep(2000);
		Assert.assertEquals("You are logged in as user \"admin\".",browser.findElement(By.xpath("//div[@translate='main.logged.message']")).getText());
	
		browser.findElement(By.xpath("//span[@translate='global.menu.entities.main']")).click();
		browser.findElement(By.xpath("//span[@translate='global.menu.entities.studenti']")).click();
		browser.findElement(By.xpath("//span[@translate='ssluzbaApp.studenti.home.createLabel']")).click();
		browser.findElement(By.id("field_indeks")).sendKeys("E1234");
		browser.findElement(By.id("field_prezime")).sendKeys("Jovanovic");
		browser.findElement(By.id("field_ime")).sendKeys("Teodora");
		browser.findElement(By.id("field_grad")).sendKeys("Novi Sad");
		browser.findElement(By.xpath("//span[@translate='entity.action.save']")).click();
		Thread.sleep(3000);
		browser.findElement(By.xpath("//span[@translate='ssluzbaApp.studenti.home.createLabel']")).click();
		browser.findElement(By.id("field_indeks")).sendKeys("E 5652");
		browser.findElement(By.id("field_prezime")).sendKeys("Jovanovic");
		browser.findElement(By.id("field_ime")).sendKeys("Danijela");
		browser.findElement(By.id("field_grad")).sendKeys("Kikinda");
		Thread.sleep(4000);
		browser.findElement(By.xpath("//span[@translate='entity.action.save']")).click();
		browser.findElement(By.xpath("//span[@translate='ssluzbaApp.studenti.home.createLabel']")).click();
		browser.findElement(By.id("field_indeks")).sendKeys("EAE 5652");
		browser.findElement(By.id("field_prezime")).sendKeys("Jovanovic");
		browser.findElement(By.id("field_ime")).sendKeys("Stevica");
		browser.findElement(By.id("field_grad")).sendKeys("Novi Sad - Alasovac");
		Thread.sleep(4000);
		browser.findElement(By.xpath("//span[@translate='entity.action.save']")).click();
		Thread.sleep(3000);
		Assert.assertEquals("E1234", browser.findElement(By.linkText("E1234")).getText());
		Assert.assertEquals("E 5652", browser.findElement(By.linkText("E 5652")).getText());
		Thread.sleep(2000);
		browser.close();
	}
	
	
}
