package DomaciObjectPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StudentiPage {

private WebDriver browser;
	
	StudentiPage(WebDriver browser){
		this.browser = browser;
		browser.navigate().to("http://localhost:9000/#/studentis");
		
	}
	
	public WebElement getCreateStudent(){
		return(browser.findElement(By.xpath("//span[@translate=\"ssluzbaApp.studenti.home.createLabel\"]")));
	}
	public WebElement getStudent(String indeks){
		return(browser.findElement(By.xpath("//div/table/tr/td/a[@text()[='match']]/../..")));
	}
	
	public WebElement getIndeks(String indeks){
		return(getStudent(indeks).findElement(By.xpath("//td[1]")));
	}
	public WebElement getIme(String indeks){
		return(getStudent(indeks).findElement(By.xpath("//td[2]")));
	}
	public WebElement getPrezime(String indeks){
		return(getStudent(indeks).findElement(By.xpath("//td[3]")));
	}
	public WebElement getGrad(String indeks){
		return(getStudent(indeks).findElement(By.xpath("//td[4]")));
	}
	public WebElement getView(String indeks){
		return(getStudent(indeks).findElement(By.xpath("//td[5]/button[1]")));
	}
	public WebElement getEdit(String indeks){
		return(getStudent(indeks).findElement(By.xpath("//td[5]/button[2]")));
	}
	public WebElement getDelete(String indeks){
		return(getStudent(indeks).findElement(By.xpath("//td[5]/button[3]")));
	}
}
