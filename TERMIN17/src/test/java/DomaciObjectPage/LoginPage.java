package DomaciObjectPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

	private WebDriver browser;
	
	LoginPage(WebDriver browser){
		this.browser = browser;
		browser.navigate().to("localhost:9000/#/login");
	}
	
	
	public WebElement getUsername(){
		return (browser.findElement(By.id("username")));
	}
	
	public void setUsername(String value){
		browser.findElement(By.id("username")).sendKeys(value);
	}
	
	public WebElement getPassword(){
		return(browser.findElement(By.id("password")));
	}
	
	public void setPassword(String value){
		browser.findElement(By.id("password")).sendKeys(value);
	}
	
	public WebElement getSubmit(){
		return(browser.findElement(By.xpath("//button[@translate='login.form.button']")));
	}
	
	public WebElement getConfirmation(){
		return(browser.findElement(By.xpath("/html/body/div[3]/div[1]/div/div/div[2]/div/div")));
	}
}

