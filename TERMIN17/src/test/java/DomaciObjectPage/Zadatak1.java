package DomaciObjectPage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Zadatak1 {

	private static WebDriver browser;
	
	@BeforeMethod
	public void seleniumSetUp(){
		browser = new FirefoxDriver();
		browser.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		browser.manage().window().maximize();
	}
	
	@Test
	public void loginTest(){
		LoginPage login = new LoginPage(browser);
		login.setUsername("admin");
		login.setPassword("admin");
		login.getSubmit().click();
		Assert.assertEquals("You are logged in as user \"admin\".",login.getConfirmation().getText());
		StudentiPage student = new StudentiPage(browser);
		student.getCreateStudent().click();
		CreateStudentPage studentCreate = new CreateStudentPage(browser);
		studentCreate.setIndeks("E2424");
		studentCreate.setIme("Marko");
		studentCreate.setPrezime("Markovic");
		studentCreate.setGrad("Novi Sad");
		studentCreate.getSave().click();
		//Assert.assertNotNull(student.getStudent("E2424"));
		student.getEdit("E2424").click();
		studentCreate.setGrad("Kraljevo");
		Assert.assertEquals("Kraljevo",student.getGrad("E2424"));
		student.getDelete("E2424");
		Assert.assertNull(student.getStudent("E2424"));
		browser.close();
	}
	
}
