package DomaciObjectPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateStudentPage {

	private WebDriver browser;
	
	CreateStudentPage(WebDriver browser){
		this.browser = browser;
		browser.navigate().to("http://localhost:9000/#/login");
	}
	
	public WebElement getIndeks(){
		return(browser.findElement(By.id("field_indeks")));
	}
	
	public void setIndeks(String value){
		getIndeks().clear();
		getIndeks().sendKeys(value);
	}
	
	public WebElement getIme(){
		return(browser.findElement(By.id("field_ime")));
	}
	
	public void setIme(String value){
		getIme().clear();
		getIme().sendKeys(value);
	}
	
	public WebElement getPrezime(){
		return(browser.findElement(By.id("field_prezime")));
	}
	
	public void setPrezime(String value){
		getPrezime().clear();
		getPrezime().sendKeys(value);
	}
	
	public WebElement getGrad(){
		return(browser.findElement(By.id("field_grad")));
	}
	
	public void setGrad(String value){
		getGrad().clear();
		getGrad().sendKeys(value);
	}
	
	public WebElement getCancel(){
		return(browser.findElement(By.xpath("//span[@translate=\"entity.action.cancel\"]")));
	}
	
	public WebElement getSave(){
		return(browser.findElement(By.xpath("/html/body/div[5]/div/div/form/div[3]/button[2]")));
	}
}
